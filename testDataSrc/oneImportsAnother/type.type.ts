import { Id } from "./id.type";

export interface Type {
  id: Id;
}
