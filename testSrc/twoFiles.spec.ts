import { test } from "@b08/test-runner";

test("two files constructor", async expect => {
  // arrange
  const srcFile = "../testData/twoFiles/constructor";
  const target = await import(srcFile);

  // act
  const result1 = target.type2("123");
  const result2 = target.type3("1234");

  // assert
  expect.deepEqual(result1, { field2: "123" });
  expect.deepEqual(result2, { field3: "1234" });
});

