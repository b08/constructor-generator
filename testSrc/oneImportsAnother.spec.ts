import { test } from "@b08/test-runner";

test("two constructors, one imports another", async expect => {
  // arrange
  const srcFile = "../testData/oneImportsAnother/constructor";
  const target = await import(srcFile);

  // act
  const result = target.type(target.id("123"));

  // assert
  expect.deepEqual(result, { id: { id: "123" } });
});

