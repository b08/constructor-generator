import { test } from "@b08/test-runner";

test("id constructor and other methods", async expect => {
  // arrange
  const srcFile = "../testData/id/constructor";
  const target = await import(srcFile);

  // act
  const id = target.companyId("123");
  const isId = target.isCompanyId(id);
  const equal = target.companyIdEquals(id, { companyId: "123" });
  const unequal = target.companyIdEquals(id, { companyId: "1234" });

  // assert
  expect.deepEqual(id, { companyId: "123" });
  expect.true(isId);
  expect.true(equal);
  expect.false(unequal);
});

