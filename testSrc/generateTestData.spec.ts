import { transformRange } from "@b08/gulp-transform";
import { dest, src } from "gulp";
import { generateConstructors } from "../src";

generateTestData()
  .catch(err => {
    console.error(err);
    process.exitCode = -1;
  });

async function generateTestData(): Promise<void> {
  const stream = src(`./testData/**/*.type.ts`)
    .pipe(transformRange(files => generateConstructors(files)))
    .pipe(dest(`./testData`));

  return waitForStream(stream);
}

export function waitForStream(stream: NodeJS.ReadWriteStream): Promise<void> {
  return new Promise((resolve, reject) => {
    stream.on("error", reject);
    stream.on("end", resolve);
  });
}
