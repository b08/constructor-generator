import { test } from "@b08/test-runner";

test("generic type constructor", async expect => {
  // arrange
  const srcFile = "../testData/genericType/constructor";
  const target = await import(srcFile);

  // act
  const id = target.genericType("123");

  // assert
  expect.deepEqual(id, { item: "123" });
});

