import { test } from "@b08/test-runner";

test("lots of fields constructor", async expect => {
  // arrange
  const srcFile = "../testData/lotsOfFields/constructor";
  const target = await import(srcFile);

  // act
  const result = target.type4("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
  const expected = {
    field1: "1",
    field2: "2",
    field3: "3",
    field4: "4",
    field5: "5",
    field6: "6",
    field7: "7",
    field8: "8",
    field9: "9",
    field10: "10"
  };

  // assert
  expect.deepEqual(result, expected);
});

test("too much fields constructor", async expect => {
  // arrange
  const srcFile = "../testData/lotsOfFields/constructor";
  const target = await import(srcFile);

  // act

  // assert
  expect.true(target.type5 == null);
});
