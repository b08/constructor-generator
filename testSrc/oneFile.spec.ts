import { test } from "@b08/test-runner";

test("one file constructor", async expect => {
  // arrange
  const srcFile = "../testData/oneFile/constructor";
  const target = await import(srcFile);

  // act
  const result = target.type1("123");

  // assert
  expect.deepEqual(result, { field1: "123" });
});
