import { ConstructorFieldModel } from "./types";

export function makeFieldInputLine(fields: ConstructorFieldModel[]): string {
  return fields.map(makeFieldInput).join(", ");
}

export function makeFieldInput(field: ConstructorFieldModel): string {
  return field.isOptional
    ? `${field.name}?: ${field.type}`
    : `${field.name}: ${field.type}`;
}
