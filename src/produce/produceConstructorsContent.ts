import { ConstructorsModel } from "./types/constructors.model";
import { GeneratorOptions } from "../types";
import { disclaimer } from "../disclaimer/disclaimer";
import { constructorFile } from "./razor/constructorFile.rzr";

export function produceConstructorsContent(model: ConstructorsModel, options: GeneratorOptions): string {
  return disclaimer(options) + constructorFile.generate(model, options);
}
