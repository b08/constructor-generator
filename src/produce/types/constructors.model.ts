import { ImportModel } from "@b08/imports-generator";

export interface ConstructorsModel {
  imports: ImportModel[];
  constructors: ConstructorModel[];
  idConstructors: IdConstructorModel[];
}

export interface IdConstructorModel {
  name: string;
  constructorName: string;
  fieldName: string; // should be the same as constructorName, but anyway
  fieldType: "string" | "number";
}

export interface ConstructorModel {
  name: string;
  constructorName: string; // first letter is lowercase
  fields: ConstructorFieldModel[];
}

export interface ConstructorFieldModel {
  name: string;
  type: string;
  isOptional: boolean;
}
