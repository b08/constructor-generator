// warning! This code was generated by @b08/razor
// manual changes to this file will be overwritten next time the code is regenerated.
import { Generator, GeneratorOptions } from "@b08/generator";
import { ConstructorsModel } from "../types";
import { imports } from "@b08/imports-generator";
import { idMethods } from "./idMethods.rzr";
import { constructor } from "./constructor.rzr";

function generateContent(model: ConstructorsModel, gen: Generator): Generator {
  gen = gen.withIndent(``, g => imports.generateContent(model.imports, g));
  for (const type of model.constructors) {
    gen = gen.eol();
    gen = gen.withIndent(``, g => constructor.generateContent(type, g));
  }
  for (const type of model.idConstructors) {
    gen = gen.eol();
    gen = gen.withIndent(``, g => idMethods.generateContent(type, g));
  }
  return gen;
}

function generate(model: ConstructorsModel, options: GeneratorOptions): string {
  return generateContent(model, new Generator(options)).toString();
}

export const constructorFile = {
  generate,
  generateContent
};
