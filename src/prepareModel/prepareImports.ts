import { flatMap } from "@b08/array";
import { ImportModel } from "@b08/imports-generator";
import { ImportedType, InterfaceModel } from "@b08/type-parser";
import { getRelativePath } from "@b08/type-parser-methods";
import { mapWith } from "@b08/functional";

export function prepareImports(types: InterfaceModel[]): ImportModel[] {
  const fileImports = types.map(createFileImport);
  const fieldImports = flatMap(types, type => createFieldsImports(type));
  return [...fileImports, ...fieldImports];
}

function createFileImport(type: InterfaceModel): ImportModel {
  const typeName = type.id.name.replace(/<.*>/, "");
  return {
    type: typeName,
    alias: typeName,
    importPath: "./" + type.id.file
  };
}

function createFieldsImports(type: InterfaceModel): ImportModel[] {
  return flatMap(type.fields, field => fieldImports(field.fieldType.importedTypes, type));
}

const fieldImports = mapWith(fieldImport);

function fieldImport(imported: ImportedType, type: InterfaceModel): ImportModel {
  const importPath = getRelativePath(type.id.folder, imported.id);
  return {
    type: imported.id.name,
    alias: imported.alias,
    importPath: importPath
  };
}


