import { InterfaceModel } from "@b08/type-parser";
import { ConstructorsModel } from "../produce/types";
import { prepareImports } from "./prepareImports";
import { prepareConstructor } from "./prepareConstructor";
import { prepareIdConstructor } from "./prepareIdConstructor";
import { lowerFirstLetter } from "./lowerFirstLetter";

export function prepareConstructorsModel(types: InterfaceModel[]): ConstructorsModel {
  const imports = prepareImports(types);
  const constructors = types.filter(t => !isId(t)).map(prepareConstructor);
  const idConstructors = types.filter(isId).map(prepareIdConstructor);
  return {
    imports,
    constructors,
    idConstructors
  };
}

function isId(type: InterfaceModel): boolean {
  if (type.fields.length > 1) { return false; }
  const lower = lowerFirstLetter(type.id.name);
  return type.id.name !== lower &&
    type.fields[0].fieldName === lower &&
    isIdType(type.fields[0].fieldType.typeName);
}

function isIdType(typeName: string): boolean {
  return typeName === "string" || typeName === "number";
}
