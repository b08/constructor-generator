import { InterfaceModel } from "@b08/type-parser";
import { IdConstructorModel } from "../produce/types";
import { lowerFirstLetter } from "./lowerFirstLetter";

export function prepareIdConstructor(type: InterfaceModel): IdConstructorModel {
  return {
    name: type.id.name,
    constructorName: lowerFirstLetter(type.id.name),
    fieldName: type.fields[0].fieldName,
    fieldType: <any>type.fields[0].fieldType.typeName
  };
}
