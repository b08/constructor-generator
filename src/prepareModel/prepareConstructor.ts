import { InterfaceModel, FieldModel } from "@b08/type-parser";
import { ConstructorModel, ConstructorFieldModel } from "../produce/types";
import { lowerFirstLetter } from "./lowerFirstLetter";

export function prepareConstructor(type: InterfaceModel): ConstructorModel {
  return {
    name: type.id.name,
    constructorName: lowerFirstLetter(type.id.name),
    fields: type.fields.map(prepareConstructorField)
  };
}

function prepareConstructorField(field: FieldModel): ConstructorFieldModel {
  return {
    name: field.fieldName,
    isOptional: field.isOptional,
    type: field.fieldType.typeName
  };
}
