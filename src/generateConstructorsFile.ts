import { ContentFile, GeneratorOptions, ParsedModel, constructorFileName } from "./types";
import { flatMap } from "@b08/array";
import { InterfaceModel } from "@b08/type-parser";
import { prepareConstructorsModel } from "./prepareModel/prepareConstructorsModel";
import { produceConstructorsContent } from "./produce/produceConstructorsContent";

export function generateConstructorsFile<T extends ContentFile>(files: ParsedModel<T>[], options: GeneratorOptions): T {
  const constructableTypes = flatMap(files, file => file.interfaces).filter(iFace => isConstructable(iFace, options));
  if (constructableTypes.length === 0) { return null; }
  const constructorsModel = prepareConstructorsModel(constructableTypes);
  const contents = produceConstructorsContent(constructorsModel, options);
  return {
    ...files[0].file,
    name: constructorFileName,
    contents
  };
}

function isConstructable(iFace: InterfaceModel, options: GeneratorOptions): boolean {
  if (iFace.fields.length === 0) { return false; }
  return options.maxFields === 0 || iFace.fields.length <= options.maxFields;
}
