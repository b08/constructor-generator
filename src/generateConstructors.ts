import { GeneratorOptions, ContentFile } from "./types";
import { parseTypesInFiles } from "@b08/type-parser";
import { groupBy } from "@b08/flat-key";
import { generateConstructorsFile } from "./generateConstructorsFile";

const defaultOptions: GeneratorOptions = {
  quotes: "\"",
  linefeed: "\n",
  maxFields: 10,
  aliasMap: {}
};

export function generateConstructors<T extends ContentFile>(files: T[], options: GeneratorOptions = {}): T[] {
  options = {
    ...defaultOptions,
    ...options
  };
  const filtered = files.filter(x => x.extension === ".ts");
  const parsed = parseTypesInFiles(filtered, options);
  const grouped = groupBy(parsed, file => file.file.folder);
  const constructors = grouped.values()
    .map(grp => generateConstructorsFile(grp, options))
    .filter(x => x != null);
  return constructors;
}
