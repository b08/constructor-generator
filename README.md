# @b08/constructor-generator, seeded from @b08/generator-seed, library type: generator
generator to create constructor functions for POJO's and other method for id-like POJO's

# constructors
For each found interface, generator produces a constructor function.\
For example: 
```
// source interface
export interface Person {
  name: string;
  age: number;
  height: number;
}

// consumer code
import { person } from "./constructor";

const p = person("Mike", 30, 180);
```

# ids
Ids are POJO objects having one field with the same name as interface, only first letter is lowercase.
For example.
```
export interface CompanyId {
  companyId: string;
}
```

For such types, generator will produce constructor and two other methods - companyIdEqual and isCompanyId.

Why would you need such id types? To combat "primitive obsession" anti-pattern.\
For example, if you use primitives (numbers or strings) as ids, then you can call method with wrong parameter and compiler will not point out that there is an error. Here is an example.
```
async function getSmth(companyId: number, personId: number ): Promise<Result> {
  const data = await getItemsByPersonId(companyId); // no compile-time error
  // process data
}

function getItemsByPersonId(personId: number): Data {...}
```

If you use object to encapsulate a primitive, compiler will highlight your mistake.
```
async function getSmth(companyId: CompanyId, personId: PersonId): Promise<Result> {
  const data = await getItemsByPersonId(companyId); // compilation error
  // process data
}

function getItemsByPersonId(personId: PersonId): Data {...}
```

# usage
```
import { generateConstructors } from "@b08/constructor-generator";
import { transformRange, transform } from "@b08/gulp-transform";
import * as changed from "gulp-changed";

const options = { lineFeed: "\n", quotes: "\"" };

export function buckets(): NodeJS.ReadWriteStream { // this is a gulp task  
  const files = ["./app/**/*.type.ts", "./app/**/*.model.ts"];
  return gulp.src(files)
    .pipe(transformRange(files => generateConstructors(files, options)))
    .pipe(changed(dest, { hasChanged: changed.compareContents }))
    .pipe(logWrittenFilesToConsole)
    .pipe(gulp.dest("./app"));
}

const logWrittenFilesToConsole = transform(file => {
  console.log(`Writing ${file.folder}/${file.name}${file.extension}`);
  return file;
});

```
# options
Along with two self explanatory options above, generator has 2 more options. \
aliasMap - absolute path mappings for type-parser\
maxFields - generator ignores interfaces with more fields than this value and does not generate constructors for them
